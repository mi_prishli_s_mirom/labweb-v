-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 28 2014 г., 20:47
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `labweb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `objects`
--

CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discipline` varchar(32) NOT NULL,
  `description` text,
  `rating` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `objects`
--

INSERT INTO `objects` (`id`, `discipline`, `description`, `rating`) VALUES
(1, 'Физкультура', 'Среди студентов института физкультуры решили провести тест на IQ. В парте выпилили три фигурки: — Квадрат, круг и треугольник. Студентам нужно было пропихнуть предложенные им фигурки в соответствующие отверстия в парте. По итогам теста студенты распределились на две группы: — Очень умные и очень сильные.', 0),
(2, 'Web-программирование', 'Дисциплина «Проектирование и разработка проблемно-ориентированных приложений (Web-программирование)» призвана содействовать знакомству студентов с компьютерными телекоммуникациями и возможными подходами к разработке гипертекстовых документов, предназначенных для публикации в глобальной компьютерной сети Internet.\r\n', 0),
(3, 'Системное программирование', 'Основными целями изучения дисциплины являются формирование общего представления работы с ядром Linux. Основное внимание уделяется изучению работы модулей, загружаемых в ядро, а также написанию программ, работающих с модулем.', 0),
(4, 'Компьютерные сети', 'Цель обучения студентов основам компьютерных сетей - обеспечить знание теоретических и практических основ в организации и функционировании компьютерных сетей и телекоммуникаций, умение применять в профессиональной деятельности распределенные данные, прикладные программы и ресурсы сетей.', 0),
(5, 'Компьютерные сети', 'Цель обучения студентов основам компьютерных сетей - обеспечить знание теоретических и практических основ в организации и функционировании компьютерных сетей и телекоммуникаций, умение применять в профессиональной деятельности распределенные данные, прикладные программы и ресурсы сетей.', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `publishedon` int(11) NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  `caption` varchar(32) NOT NULL,
  `text` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
