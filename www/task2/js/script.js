/**
 * Created by vetal_000 on 26.11.2014.
 */
$(function() {
    var height = 285 + 60;
    var lenght = $('.discipline').length;
    $('.inner').css("min-height", (height*lenght)+"px");

    setTimeout(view, 500);

    var element = [];
    var truel = [];
    function view() {
        var height1 = height;
        var height2 = 0;
        $('.discipline').each(function(i) {
            truel[i] = $(this);
            element[i] = $(this).find('.num-like').html();
            $(this).addClass('element-'+(i+1)).css({
                "opacity" : "1",
                "top" : height2
            });
            height2 += height1;
        });
    }

    $('.likeplus').click(function() {
        var id = $(this).find('.id').val();
        var rating = $(this).find('.num-like');
        var ratingVal = rating.html();

        var top = $(this).closest('.discipline').css("top");
        top = top.replace("px", "");
        $.ajax({
            url: "php/controller.php",
            type: "POST",
            data: {
                key: '1', //Код операции в контроллере
                id: id,
                rating: ratingVal
            },
            success: function(data) {
                rating.html(data);
                updateContent2(top);
            }
        });
    });

    function updateContent2(top) {
        var position = top / height;
        element[position]++;

        element = element.sort(function (a, b) {
            return b - a;
        });
        var temp = 0;
        for (var i = 0; i < element.length; i++) {
            var like = truel[i].find('.num-like').html();
            if (like != element[i]) {
                for (var j = 0; j < element.length; j++) {
                    if (like == element[j]) {
                        temp = truel[i];
                        truel[i] = truel[j];
                        truel[j] = temp;
                        truel[i].css("top", (i * height) + "px");
                        truel[j].css("top", (j * height) + "px");
                        return true;
                    }
                }
            }
        }
    }
});