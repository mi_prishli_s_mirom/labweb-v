<?php
/**
 * Created by PhpStorm.
 * User: vetal_000
 * Date: 26.11.2014
 * Time: 11:20
 */

$connect = mysql_connect("127.0.0.1", "root", "");

if (!$connect) {
    echo "Ошибка подключения к базе данных: <br>".mysql_error();
    exit();
}
if (!@mysql_select_db("labweb", $connect)) {
    echo "Ошибка выбора базы данных: <br>".mysql_error();
    exit();
}

class Discipline {
    public $id;
    private $name;
    private $description;
    private $rating;

    function __construct($row) {
        $this->id = $row['id'];
        $this->name = $row['discipline'];
        $this->description = $row['description'];
        $this->rating = $row['rating'];
    }

    function __toString() {
        $replace['id'] = $this->id;
        $replace['discipline'] = $this->name;
        $replace['description'] = $this->description;
        $replace['rating'] = $this->rating;
        return (string)new Chunk("discipline", $replace);
    }

    function like() {
        $this->rating++;
        $query = mysql_query("UPDATE objects SET rating=$this->rating WHERE id=$this->id");
        if($query) {
            return $this->rating;
        } else {
            return mysql_error();
        }
    }
}

class Chunk {
    private $name;
    private $replace;

    function __construct($name, $replace) {
        $this->name = $name;
        $this->replace = $replace;
    }

    function __toString() {
        return $this->getChunk();
    }

    private function getChunk() {
        $tpl = file_get_contents("view/".$this->name.".tpl");
        if(!empty($this->replace)) {
            foreach($this->replace as $key => $val) {
                $tpl = str_replace("{".$key."}", $val, $tpl);
            }
        }
        return $tpl;
    }
}