<div class="discipline" id="id-{id}">
    <h2>{discipline}</h2>
    <div class="cont">
        <p class="description"><u>Описание дисциплины:</u><br> <i>{description}</i></p>
        <div class="like">
            <div class="likeplus">
                <span class="pretext">Оценить: </span>
                <img src="img/like.png" alt="{rating}" />
                <span class="num-like">{rating}</span>
                <input class="id" type="hidden" value="{id}" />
            </div>
        </div>
    </div>
</div>