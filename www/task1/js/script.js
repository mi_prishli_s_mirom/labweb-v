/**
 * Created by vetal_000 on 28.11.2014.
 */
$(function() {
    var i1 = "css/style-1.css";
    var i2 = "css/style-2.css";
    $('.i1').click(function() {
            $('.scheme-color').attr("href", i1);
    });
    $('.i2').click(function() {
        $('.scheme-color').attr("href", i2);
    })

    $('.j1').click(function() {
        $('.discipline').css({
            "width" : "200px",
            "display" : "inline-block",
            "vertical-align" : "top",
            "margin-right" : "10px"
        });
    });
    $('.j2').click(function() {
        $('.discipline').css({
            "width" : "98%",
            "display" : "block",
            "vertical-align" : "top",
            "margin-right" : "0"
        });
    });
});
