<!DOCTYPE html>
<html>
<head>
    <title>taskManager</title>
    <script src="../library/jquery-2.1.0.min.js"></script>
    <script src="taskManager.js"></script>
    <script src="//ajax.aspnetcdn.com/ajax/jquery.ui/1.10.3/jquery-ui.min.js"></script>
    <link href="taskManager.css" rel="stylesheet" />
</head>

<body>
    <header><h2>Менеджер заданий</h2><h4>Управление заданиями</h4></header>
    
    <section>
        <div id="taskTable">
        </div>
    </section>
    <input id="clearTable" type="submit" value="Очистить таблицу" />
    <p>Для управления заданиями просто перенесите задание в следующую колонку. Все процессы регистрирует журнал.</p>
    <h4>Регистрация задания</h4>

    <section>
            <p><input type="text" placeholder="Заголовок задания" name="taskCaption" id="taskCaption" class="field" /></p>
            <p><textarea placeholder="Текст задания" rows="5" cols="35" name="taskCont" id="taskCont" class="field"></textarea></p>
            <p><input type="submit" value="Добавить" name="taskSub" id="taskSub" /></p>
    </section>
    <br>
    <br>
    <br>
    <h4>Журнал</h4>
    <span id="journal"></span>
</body>
</html>