$(function() {
    
    var tableWidth = $('#taskTable').width();
    
    function refresh() {
        $.get("test.php", function(data) {
            $('#taskTable').empty();
            $('#taskTable').html(data);
            
            $.post("journal.php", function(data) {
                $('#journal').empty();
                $('#journal').html(data);
            });
            
            
            var blockWidth = $('.line1').width();
            $('#headTable div').width(blockWidth-26);
            
            colTask();
            
            $('.line').sortable({
                placeholder: "empty",
                containment: '.sampleClass',
                cursor: 'move',
                beforeStop: function(event, ui) {
                    ui.placeholder.parent().css({"background-color" : "transparent"});
                },
                over: function(event, ui) {
                    ui.placeholder.parent().css({"background-color" : "green"});
                },
                out: function(event, ui) {
                    ui.placeholder.parent().css({"background-color" : "red"});
                },
                change: function(event, ui) {
                    ui.placeholder.parent().css({"background-color" : "green"});
                },
                update: function() {
                    $.post("journal.php", function(data) {
                        $('#journal').empty();
                        $('#journal').html(data);
                    });
                    colTask();
                },
                opacity: '0.5',
                revert: true
            });
            $('.line1').sortable({
                connectWith: ".line2"
            });
            $('.line2').sortable({
                receive: function(event, ui) {
                    var info = $(this).sortable("serialize");
                    var caption = ui.item.html();
                    $.post("update.php", {info: info, timestamp: $.now(), status: '2', caption: caption}, function(data) {
                        
                    });
                },
                update: function(event, ui) {alert();
                    var info = $(this).sortable("serialize");
                    var caption = ui.item.html();
                    alert(caption);
                },
                connectWith: ".line3"
            });
            $('.line3').sortable({
                receive: function(event, ui) {
                    var info = $(this).sortable("serialize");
                    var caption = ui.item.html();
                    $.post("update.php", {info: info, timestamp: $.now(), status: '3', caption: caption}, function(data) {
                        
                    });
                }
            });
        });
    }
    
    function colTask() {
        $("#backLog").empty().html("Подготовленные: " + $(".line1 > *").length);
        $("#inProgress").empty().html("В работе: " + $(".line2 > *").length);
        $("#done").empty().html("Исполненные: " + $(".line3 > *").length);
    }

    function addTask(caption, text) {
        $.get("action.php", {caption: caption, text: text}, function (data) {
            if (data == 'Такой заголовок задания уже есть, выберите другой')
                alert(data);
            refresh();
        });
    }
    
    $(window).load(function() {
        refresh();
    });
    
    
    $('#clearTable').click(function() {
        if (confirm('Вы действительно хотите удалить все записи безвозвратно?')) {
            if (confirm('Еще раз подумайте... если вы хотите сделать это только лишь для проверки функции отчистки, поверьте мне, она работает. Удаляем?')) {
                $.post("clearTable.php", function() {
                    refresh();
                });
            } else {
                return false;
            }
        } else {
            return false;
        }
    });
    
    $("#taskSub").click(function() {
        var caption = $("#taskCaption").val();
        if (caption.length === 0) {
            alert("Ошибка! Заполните все поля.");
            return false;
        }
        if (caption.length > 30) {
            alert("Ошибка! Размеры заголовка не должны превышать 30 символов.");
            return false;
        }
        var text = $("#taskCont").val();
        if (text.length === 0) {
            alert("Ошибка! Заполните все поля.");
            return false;
        }
        addTask(caption, text);
        $("#taskCaption").val('');
        $("#taskCont").val('');
    });
});